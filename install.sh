#!/usr/bin/env bash

set -eof pipefail

#The MIT License (MIT)
#
#Copyright (c) 2014 Sebastian Dahlgren
#
#Permission is hereby granted, free of charge, to any person obtaining a copy
#of this software and associated documentation files (the "Software"), to deal
#in the Software without restriction, including without limitation the rights
#to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#copies of the Software, and to permit persons to whom the Software is
#furnished to do so, subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in all
#copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
##OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
#SOFTWARE.

#
# Install script for Vinter
#

# Set global variables
backup_dir="${HOME}/.vinter-backup.$(date +%Y%m%dT%H%M%S)"
source_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
target_dir=${HOME}
install_dir="/opt"

###########################
#
# Functions
#
###########################

# Print usage information
print_usage()
{
  echo -e "Usage: install.sh [OPTIONS]\n"
  echo "OPTIONS:"
  echo -e "--help\t\tShow this help"
}

# Backup a file
#
# Args:
# - File to backup
backup()
{
  if [ ! -d ${backup_dir} ] ; then
    mkdir -p ${backup_dir}
  fi

  if [ -e ${target_dir}/${1} ] ; then
    echo "Backing up ${target_dir}/${1} to ${backup_dir}/${1}"
    cp -r ${target_dir}/${1} ${backup_dir}/${1}
  fi
}

# Link files
#
# Args:
# - File to link to target
link()
{
  echo "Linking ${source_dir}/${1} to ${target_dir}/.${1}"
  ln -s ${source_dir}/${1} ${target_dir}/.${1}
}

# Remove files, dirs or links
#
# Args:
# - Target
remove()
{
  if [ -L ${target_dir}/${1} ] ; then
    echo "Unlinking ${target_dir}/${1}"
    unlink ${target_dir}/${1}
  elif [ -d ${target_dir}/${1} ] ; then
    echo "Removing ${target_dir}/${1}"
    rm -rf ${target_dir}/${1}
  elif [ -f ${target_dir}/${1} ] ; then
    echo "Removing ${target_dir}/${1}"
    rm -rf ${target_dir}/${1}
  fi
}

###########################
#
# Argument parsing
#
###########################

if [ "${1}" == "--help" ] ; then
  print_usage
  exit 0
fi

###########################
#
# Main program
#
###########################

echo "> REBUILDING VIM"

#Install Code Checkers for C,C++,nasm, Ruby, Perl and Python
apt-get install build-essential cmake git -y
apt-get install python-dev python3-dev python3-pip -y
apt-get install gcc -y
apt-get install flawfinder -y
apt-get install nasm -y
apt-get install ruby ruby-dev -y
apt-get install perl ncurses-dev -y
pip3 install prospector
apt-get update -y && apt-get upgrade -y
gem install rubocop
cpan Perl::Critic
#--------------------------------------------------------------

apt-get remove --purge vim-* -y
cd ${install_dir}
git clone https://github.com/vim/vim.git
cd /opt/vim/
./configure --with-features=huge \
            --enable-multibyte \
            --enable-rubyinterp=yes \
            --enable-perlinterp=yes \
	    	--enable-luainterp=yes \
            --enable-gui=gtk2 \
            --enable-cscope \
            --enable-python3interp=yes \
            --with-python3-config-dir=/usr/lib/python3.6/config-3.6m-x86_64-linux-gnu \
            --prefix=/usr/local
make VIMRUNTIMEDIR=/usr/local/share/vim/vim81
make install
cd ..
rm -R vim


echo "> INSTALLING Vinter"

# Backup local vim files
echo -e "\n>> CREATING BACKUPS"
echo "Backing up all your old vim files to ${backup_dir}"
backup ".vim"
backup ".vimrc"
backup ".vimrc.local"
backup ".vimrc.plugins.local"

echo -e "\n>> CLEAN UP"
echo "Clean up old vim configuration"
remove ".vim"
remove ".vimrc"

# Link vim files
echo -e "\n>> INSTALL BASE CONFIGURAION"
echo "Installing Vinter configuration files"
link "vim"

echo "Creating example .vimrc.local (${target_dir}/.vimrc.local.example)"
if [ -e ${target_dir}/.vimrc.local.example ] ; then
  rm ${target_dir}/.vimrc.local.example > /dev/null 2>&1
fi
cp ${source_dir}/vimrc.local.example ${target_dir}/.vimrc.local.example

# Install Vundle
echo -e "\n>> INSTALL DEPENDENCIES AND PLUGINS"
echo "Installing Vundle for plugin management"
if [ -d ${target_dir}/.vim/bundle/Vundle.vim ] ; then
    cd ${target_dir}/.vim/bundle/Vundle.vim ; git pull ; cd -
else
    git clone https://github.com/gmarik/Vundle.vim.git ${target_dir}/.vim/bundle/Vundle.vim
fi

#Setup YouCompleteMe For C/C++
git clone https://github.com/Valloric/YouCompleteMe.git ${target_dir}/.vim/bundle/YouCompleteMe
cd ~/.vim/bundle/YouCompleteMe
git submodule update --init --recursive
./install.py --clang-completer



# Install all plugins
vim +PluginInstall +qall

echo -e "\n> DONE"
echo "Done with the installation. Happy hacking!"

exit 0
