###### The following is a remake of [Sebdah's Vim Config](https://github.com/sebdah/vim-ide) to support different languages and using the Gruvbox theme
# The Following plugins are included using Vundle:
## NOTE THE FOLLOWING REPO IS NO LONGER MAINTAINED.PROJECT RELIES ON TOO MANY NON ASYNC PLUGINS WHICH MAKES VIM QUITE SLOW.


* [EditorConfig](https://github.com/editorconfig/editorconfig-vim)
* [ControlP](https://github.com/ctrlpvim/ctrlp.vim)
* [Nerd Treee](https://github.com/scrooloose/nerdtree)
* [Vim_Tmux](https://github.com/christoomey/vim-tmux-navigator)
* [YCM](https://github.com/Valloric/YouCompleteMe)
* [Vim Airline](https://github.com/vim-airline/vim-airline)
* [Vim Gas](https://github.com/shirk/vim-gas)
* [Syntastic](https://github.com/scrooloose/syntastic)
* [PhpComplete](https://github.com/shawncplus/phpcomplete.vim)
* [Powershell](https://github.com/PProvost/vim-ps1)
* [Airline themes](https://github.com/vim-airline/vim-airline-themes) (Currently using base16color)

# Supported Languages:

* Python
* Ruby
* Perl
* C
* C++
* PHP
* Powershell
* Nasm
* SQL
* ... and more

# Installation Requirements:
* Nothing(Vim will be compiled during this script)
