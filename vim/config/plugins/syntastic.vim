" Go fixes
" This resolves a bug which makes saving Go files slow
" let g:syntastic_enable_perl_checker = 1
let g:syntastic_mode_map = { 'mode': 'active', 'passive_filetypes': ['html'] }
let g:syntastic_c_include_dirs = ["includes", "headers"]
let g:syntastic_python_checkers = ['prospector']
let g:syntastic_c_checkers = ['gcc','flawfinder','cppcheck']
let g:syntastic_cpp_checkers = ['gcc','flawfinder','vera++']
let g:syntastic_ruby_checkers = ['rubocop','ruby-lint']
let g:syntastic_asm_checkers = ['gcc']
let g:syntastic_nasm_checkers = ['nasm']
let g:syntastic_perl_checkers = ['perlcritic','perl','podchecker']
let g:syntastic_go_checkers = ['golint', 'govet', 'errcheck']
let g:syntastic_javascript_checkers = ['eslint']
